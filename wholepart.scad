include <honeycomb.scad>

include <raspberrypicasestand.scad>;
$fn=80;
module Case(){
	interfaceSide();
	ScrewMount(30); // screw mount at 0, 0
	translate([140,0,0]) ScrewMount(30); // screw mount at 140, 0
	translate([140,100,0]) ScrewMount(30); // screw mount at 140, 100
	
	sdrWall();
	translate([0,100,0])ScrewMount(30);
	translate([2,65,0]) Wall(90,thickness=2.5); // side between the pi and sdr
	translate([0,66.5,0])ScrewMount(30); 
	translate([94,66.5,0])ScrewMount(30);
	
	SdCardWall();
	
	translate([0,68,0]) rotate([0,0,90]) CoaxHoleWall();
	
	translate([140,2,0]) rotate([0,0,90]) EthernetUsbWall();
	
	translate([0, 80.5, 2]) rtlsdrHolder();
	
	difference(){
		translate([5,5,0]) piStand();
		mounting2();
	}
	
	difference(){
		cube([140,100,piSize[2]]);  //floor
		mounting1();
	}
}
module mounting1(){
		
		translate([100,0,-3])mount();
		translate([100,65,-2])mount();
		mount();
		translate([20,43,-2])mount();	
}
module mounting2(){
		mount();
		translate([20,43,-2])mount();	
}
module mount(){
	translate([12,15,-1])cylinder(h=10,r=3);
	translate([15,15,-1])cylinder(h=10,r=2.2);
	translate([16,15,-1])cylinder(h=10,r=2.2);	
	translate([17,15,-1])cylinder(h=10,r=2.2);
	translate([18,15,-1])cylinder(h=10,r=2.2);	
}
module sdrWall(){
		translate([2,98,0]) Wall(); // screw mount at 2, 98	
}
module Wall(length=136, height=30, thickness=3) {
	cube([length, thickness, height]);
}
module ScrewMount(height){
	difference(){
		
		cylinder(h=height, r1=standoffSize[1], r2=standoffSize[2]);
		translate([0,0,11]) cylinder(h=height- 10,   r1=screwHoleSize[1], r2=screwHoleSize[2]);
	}
}
module CoaxHoleWall(){
	difference(){
		translate([0,0,0]) Wall(31, thickness=3); // sdr side with coax
		translate([16,5,13]) rotate([90,0,0]) cylinder(h=8, r=8);
	}
}

module rtlsdrHolder(){
	cube([71,5,5]);
	translate([0,13,0]) cube([70,3,6]);
	translate([0,-10,0]) cube([70,3,6]);
	translate([72.5,-13,0]) cube([2.5,30,9]);
	translate([72.5,-13,0]) cube([2.5,6,17]);
	translate([72.5,11.5,0]) cube([2.5,6,17]);
}
module EthernetUsbWall(){

		difference(){
			Wall(98); // sdcard wall 
			USBEthernetHole();
		}

}

module USBEthernetHole(){
	translate([10,5,5]) rotate([90,0,0]) cube([20,20,20]);
	translate([50,5,5]) rotate([90,0,0]) cube([30,8,20]);

}

module SdCardWall(){
		difference(){
			translate([0,2,0]) rotate([0,0,90]) Wall(63); // sdcard wall 
			SdCardHole();
		}	
		
}
module SdCardHole(){
	translate([-4,24,6]) rotate([90,0,90]) cube([15,4,10]);	
		
}
module interfaceSide(){
	difference(){
		translate([2,-1.5,0]) Wall(); // side with power
		HolesOfInterfaceSide();
	}
}

module HolesOfInterfaceSide(){
	translate([8,-8, 8]) cube([14,20,10]);
	translate([25,-9, 8]) cube([25,20,14]);
}
module anchored_cube(prefix, size=[1,1,1]) {

     cube(size, center=false);
     point(prefix + "_" + "my_anchor", [1, 1, 1]);
 }

Case();