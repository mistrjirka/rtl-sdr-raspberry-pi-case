include <honeycomb.scad>
difference() {translate ([-5,-5, 0]) roundedcube([150,110,3], false, 1, "z");;
	holes();	
}
sizeOfHoles = 1.3525;
$fn=80;
module hole(){
	translate ([0,0,-5]) cylinder(h=10,r=sizeOfHoles);
}
module holes(){
	translate([0,0,0]) hole();	
	translate([140,0,0]) hole();
	translate([0,100,0]) hole();
	translate([140,0,0]) hole();	
	translate([140,100,0]) hole();
	translate([0,66.5,0]) hole();
	translate([94,66.5,0]) hole();
	translate([5,10,0]) linear_extrude(4) {
		honeycomb(70, 37, 5, 3);
	}
	translate([115,5,0]) linear_extrude(4) {
		honeycomb(1, 85, 4, 2);
	}






}
font1 = "Liberation Sans"; // here you can select other font type
content = "RPI 3 RTL-SDR box";

translate ([100,90,2]) {
rotate ([0,0,270]) {
linear_extrude(height = 3) {
text(content, font = font1, size = 7, direction = "ltr", spacing = 1 );
}
}
}

content2 = "designed by Jiří Svítil";

translate ([80,85,2]) {
	rotate ([0,0,180]) {
		linear_extrude(height = 3) {
			text(content2, font = font1, size = 5, direction = "ltr", spacing = 1 );
		}
	}
}
$fs = 0.01;


module roundedcube(size = [1, 1, 1], center = false, radius = 0.5, apply_to = "all") {
	// If single value, convert to [x, y, z] vector
	size = (size[0] == undef) ? [size, size, size] : size;

	translate_min = radius;
	translate_xmax = size[0] - radius;
	translate_ymax = size[1] - radius;
	translate_zmax = size[2] - radius;

	diameter = radius * 2;

	module build_point(type = "sphere", rotate = [0, 0, 0]) {
		if (type == "sphere") {
			sphere(r = radius);
		} else if (type == "cylinder") {
			rotate(a = rotate)
			cylinder(h = diameter, r = radius, center = true);
		}
	}

	obj_translate = (center == false) ?
		[0, 0, 0] : [
			-(size[0] / 2),
			-(size[1] / 2),
			-(size[2] / 2)
		];

	translate(v = obj_translate) {
		hull() {
			for (translate_x = [translate_min, translate_xmax]) {
				x_at = (translate_x == translate_min) ? "min" : "max";
				for (translate_y = [translate_min, translate_ymax]) {
					y_at = (translate_y == translate_min) ? "min" : "max";
					for (translate_z = [translate_min, translate_zmax]) {
						z_at = (translate_z == translate_min) ? "min" : "max";

						translate(v = [translate_x, translate_y, translate_z])
						if (
							(apply_to == "all") ||
							(apply_to == "xmin" && x_at == "min") || (apply_to == "xmax" && x_at == "max") ||
							(apply_to == "ymin" && y_at == "min") || (apply_to == "ymax" && y_at == "max") ||
							(apply_to == "zmin" && z_at == "min") || (apply_to == "zmax" && z_at == "max")
						) {
							build_point("sphere");
						} else {
							rotate = 
								(apply_to == "xmin" || apply_to == "xmax" || apply_to == "x") ? [0, 90, 0] : (
								(apply_to == "ymin" || apply_to == "ymax" || apply_to == "y") ? [90, 90, 0] :
								[0, 0, 0]
							);
							build_point("cylinder", rotate);
						}
					}
				}
			}
		}
	}
}