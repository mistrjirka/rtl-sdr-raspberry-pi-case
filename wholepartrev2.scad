include <honeycomb.scad>

include <raspberrypicasestand.scad>;
$fn=80;
module Case(){
	interfaceSide();
	ScrewMount(40); // screw mount at 0, 0
	translate([120,0,0]) ScrewMount(40); // screw mount at 140, 0
	translate([120,100,0]) ScrewMount(40); // screw mount at 140, 100
	
	translate([2,97,0]) sdrWall();
	translate([0,97,0])ScrewMount(40);
	translate([2,62,0]) Wall(90,thickness=2.5); // side between the pi and sdr
	translate([0,63.5,0])ScrewMount(40); 
	translate([94,63.5,0])ScrewMount(40);
	
	SdCardWall();
	
	translate([0,65,0]) rotate([0,0,90]) CoaxHoleWall();
	
	translate([120,2,0]) rotate([0,0,90]) EthernetUsbWall();
	
	translate([0, 78, 2]) rtlsdrHolder();
	
	difference(){
		translate([2,2,0]) piStand();
		mounting2();
	}
	
	difference(){
		cube([120,100,piSize[2]]);  //floor
		mounting1();
	}
}
/* these are the main modules that are used almost in every module */
module Wall(length=116, height=40, thickness=3) {
	cube([length, thickness, height]);
}
module ScrewMount(height){
	difference(){
		
		cylinder(h=height, r1=standoffSize[1], r2=standoffSize[2]);
		translate([0,0,11]) cylinder(h=height- 10,   r1=screwHoleSize[1], r2=screwHoleSize[2]);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

module mounting1(){
		
		translate([90,0,-3])mount();
		translate([90,65,-2])mount();
		mount();
		translate([20,43,-2])mount();	
}
module mounting2(){
		mount();
		translate([20,43,-2])mount();	
}
module mount(){
	translate([12,15,-1])cylinder(h=10,r=3);
	translate([15,15,-1])cylinder(h=10,r=2.2);
	translate([16,15,-1])cylinder(h=10,r=2.2);	
	translate([17,15,-1])cylinder(h=10,r=2.2);
	translate([18,15,-1])cylinder(h=10,r=2.2);	
}
module sdrWall(){
	difference(){
		Wall();
		difference(){
			rotate ([0,-90,90]) translate([8,-65,-5]) linear_extrude(7) {
				honeycomb(5, 60, 7, 3);
				
			}
			SdrWallBlockage();		
		}
	}
}
module SdrWallBlockage(){
	translate([0,-10,20])cube([100,20,20]);	
	translate([-5,-2,0])cube([10,10,20]);
	translate([0,-2,12])cube([10,10,20]);
}
module CoaxHoleWall(){
	difference(){
		translate([0,0,0]) Wall(30, thickness=3); // sdr side with coax
		translate([16,5,13]) rotate([90,0,0]) cylinder(h=8, r=8);
	}
}

module rtlsdrHolder(){
	cube([71,5,5]);
	translate([0,13,0]) cube([70,3,6]);
	translate([0,-10,0]) cube([70,3,6]);
	translate([72.5,-13,0]) cube([2.5,30,9]);
	translate([72.5,-13,0]) cube([2.5,6,17]);
	translate([72.5,11.5,0]) cube([2.5,6,17]);
}
module EthernetUsbWall(){

		difference(){
			Wall(96); // sdcard wall 
			USBEthernetHole();
		}

}

module USBEthernetHole(){
	translate([10,5,5]) rotate([90,0,0]) cube([20,20,20]);
	translate([50,5,5]) rotate([90,0,0]) cube([40,15,20]);

}

module SdCardWall(){
	difference() {
		translate([0,2,0]) rotate([0,0,90]) Wall(60); // sdcard wall 
		difference(){
			rotate ([0,270,00]) translate([7,5,-1]) linear_extrude(5) {
				honeycomb(20, 50, 4.3, 2.9);
			}
			
			translate([-10,62,1]) SdCardWallHoneycombBlockage (); 
		}
	}
}

module SdCardWallHoneycombBlockage (){ // very bad practice but it works (someone better pls fix)
	cube([30,20,40]);
	translate([-10,-4,8]) cube([30,6,5]);
	translate([-10,-4,21]) cube([30,6,5]);
	translate([-10,-4,32]) cube([30,6,5]);
}

module interfaceSide(){
	difference(){
		translate([2,-1.5,0]) Wall(); // side with power
		HolesOfInterfaceSide();
	}
}

module HolesOfInterfaceSide(){
	translate([8,-8, 7]) cube([14,20,10]);
	translate([25,-9, 7]) cube([24,20,14]);
	translate([52.5,-9, 7]) cube([12,12,12]);
}
module anchored_cube(prefix, size=[1,1,1]) {
		
     cube(size, center=false);
     point(prefix + "_" + "my_anchor", [1, 1, 1]);
 }

Case();