$fn=60;
piSize = [85,56,2];
screwHoleSize = [4,1.3525,1.3525];
standoffSize = [6,3.1, 3.1];
heigthOfPiStand = 10;
cube(piSize, 0);
standoff(3.5,3.5,2);
standoff(58+3.5,3.5,2);
standoff(58+3.5,3.5+49,2);
standoff(3.5,3.5+49,2);
module standoff(x,y,z){
    difference(){
        translate([x, y, z+(standoffSize[0]-screwHoleSize[0])]) cylinder(h=screwHoleSize[0],   r1=screwHoleSize[1], r2=screwHoleSize[2]);
        translate([x, y, z]) cylinder(h=standoffSize[0], r1=standoffSize[1], r2=standoffSize[2]);
    }
   
}
